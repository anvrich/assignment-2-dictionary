%include "colon.inc"

section .data

colon "first", first
db "value 1", 0

colon "second", second
db "value 2", 0

colon "third", third
db "value 3", 0

colon "fourth", fourth
db "value 4", 0

colon "fifth", fifth
db "value 5", 0

colon "second word", second_word
db "value for second word", 0 

colon "third test case", third_test_case
db "value for third test case", 0
