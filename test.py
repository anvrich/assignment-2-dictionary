import subprocess
inputs = [
    "first", 
    "second", 
    "third", 
    "fourth", 
    "fifth", 
    "", 
    "random", 
    "a" * 255,
    "b" * 256,
    "second word", 
    "third test case"  
]

outputs = [
    "value 1", 
    "value 2", 
    "value 3", 
    "value 4", 
    "value 5", 
    "", 
    "", 
    "",  
    "",  
    "value for second word",  
    "value for third test case"  
]

errors = [
    "", 
    "", 
    "", 
    "", 
    "", 
    "!!! not found !!!", 
    "!!! not found !!!", 
    "!!! not found !!!", 
    "!!! key should be < 256 chars !!!",
    "", 
    ""  
]

return_codes = [0, 0, 0, 0, 0, 1, 1, 1, 2, 0, 0]

num_failures = 0

print("Starting tests")
print("-------------------")

for i in range(len(inputs)):
    process = subprocess.Popen(["./result"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate(input=inputs[i].encode())
    # print(process.returncode)
    stdout = stdout.decode().strip()
    stderr = stderr.decode().strip()

    if stdout == outputs[i] and stderr == errors[i] and process.returncode == return_codes[i]:
        print("Test %s passed" % i)
    else:
        num_failures += 1
        print("Test %s failed" % i)
        if stdout != outputs[i]:
            print("Wrong stdout " + stdout + ", expected " + outputs[i], f"Return code: {return_codes[i]}")
        if stderr != errors[i]:
            print("Wrong stderr " + stderr + " expected " + errors[i], f"Return code: {return_codes[i]}")
    print("-------------------")

if num_failures == 0:
    print("All tests passed")
else:
    print("%d tests failed" % num_failures)
