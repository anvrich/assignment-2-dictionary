%include "colon.inc"
%include "dict.inc"
%include "lib.inc"
%include "words.inc"


%define BUFFER_SIZE 255

global _start

section .rodata

input_error: db "!!! key should be < 256 chars !!!", 0
find_error: db "!!! not found !!!", 0

section .bss
    buffer: resb (BUFFER_SIZE+1)

section .text
_start:
    ; Read a word from stdin into input_buffer
    mov rdi, buffer
    mov rsi, BUFFER_SIZE
    call read_word

    test rax, rax
    jz .input_error

    ; Find the word in the dictionary
    mov rdi, buffer
    mov rsi, dict
    call find_word

    test rax, rax
    jz .find_error

    mov rdi, rax
    add rdi, 8
    call string_length
    add rdi, rax
    inc rdi
    call print_string
    mov rdi, 0
    jmp exit

    .input_error:
        mov rdi, input_error
        call print_error
        mov rdi, 2
        jmp exit

    .find_error:
        mov rdi, find_error
        call print_error
        mov rdi, 1
        jmp exit
